#!/usr/bin/env python
from crhelper import CfnResource
import boto3
import logging


logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL')

try:
    session = boto3.session.Session()
    client = boto3.client('route53')
except Exception as e:
    helper.init_failure(e)


def get_zone_id(event):
    try:
        return event['ResourceProperties']['HostedZoneID']
    except Exception as e:
        helper.init_failure(e)


def _get_hosted_zone_by_id(zone_id):
    return client.get_hosted_zone(
        Id=zone_id
    )


@helper.create
def create(event, context):
    logger.info("Got Create")
    # Optionally return an ID that will be used for the resource PhysicalResourceId,
    # if None is returned an ID will be generated. If a poll_create function is defined
    # return value is placed into the poll event as event['CrHelperData']['PhysicalResourceId']
    #
    # To add response data update the helper.Data dict
    # If poll is enabled data is placed into poll event as event['CrHelperData']
    # Grab data from environment
    zone_id = get_zone_id(event)
    response = _get_hosted_zone_by_id(zone_id)
    helper.Data.update({
        'Name': response['HostedZone']['Name'].rstrip('.'),
    })
    return get_zone_id


@helper.update
def update(event, context):
    logger.info("Got Update")
    # If the update resulted in a new resource being created, return an id for the new resource. CloudFormation will send
    # a delete event with the old id when stack update completes
    zone_id = get_zone_id(event)


@helper.delete
def delete(event, context):
    logger.info("Got Delete")


def handler(event, context):
    helper(event, context)
